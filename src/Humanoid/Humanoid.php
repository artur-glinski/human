<?php

namespace Sda\Example\Humanoid;

abstract class Humanoid {

	/**
	 * @var type
	 */
	protected $actualState;

	/**
	 * Description
	 * @return type
	 */
	public function sleeep() {
		$this->actualState = 'human śpi<br>';
		//$this->getState();
	}

	/**
	 * Description
	 * @return type
	 */
	public function eat() {
		$this->actualState = 'human żre<br>';
		//$this->getState();
	}

	/**
	 * Description
	 * @return type
	 */
	public function work($string = '') {
		$this->actualState = 'human pracuje <br>' . $string;
		//$this->getState();
	}

	/**
	 * Description
	 * @return type
	 */
	public function getState() {
		return $this->actualState;
	}

}


// sila int facet 100 kobieta 50
// wlosy short long string