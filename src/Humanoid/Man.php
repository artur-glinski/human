<?php

namespace Sda\Example\Humanoid;

class Man extends Humanoid {

	private $hair;

	private $power;

	public function __construct($hair, $power) {
		$this->hair = $hair;
		$this->power = $power;
	}

}
