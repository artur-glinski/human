<?php

namespace Sda\Example\Humanoid;

class Woman extends Humanoid {

	/**
	 * @var type
	 */
	private $hair;

	public function __construct($hair) {
		$this->hair = $hair;
	}

	public function cook() {
		$this->work('i gotuje<br>');
	}

	// public function hair() {
	// 	echo "";
	// }


}
